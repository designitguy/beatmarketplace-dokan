<?php
  /*   $img_kses = apply_filters( 'dokan_product_image_attributes', array(
        'img' => array(
            'alt'    => array(),
            'class'  => array(),
            'height' => array(),
            'src'    => array(),
            'width'  => array(),
        ),
    ) );

    $row_actions_kses = apply_filters( 'dokan_row_actions_kses', array(
        'span' => array(
            'class' => array(),
        ),
        'a' => array(
            'href'    => array(),
            'onclick' => array(),
        ),
    ) );

    $price_kses = apply_filters( 'dokan_price_kses', array(
        'span' => array(
            'class' => array()
        ),
    ) ); */
	gloabal $post;
?>
<tr class="<?php echo esc_attr( $tr_class ); ?>">
    <td class="dokan-product-select">
		<label for="cb-select-<?php echo esc_attr( $loop->post->ID ); ?>"></label>
		<input class="cb-select-items dokan-checkbox" type="checkbox" name="bulk_products[]" value="<?php echo esc_attr( $loop->post->ID ); ?>">
	</td>
	<td data-title="<?php esc_attr_e( 'Image', 'dokan-lite' ); ?>">
	<?php $imageid = get_post_meta($loop->post->ID, 'license_image', true); 
	$imageurl = wp_get_attachment_image_src($imageid); ?>
	<?php if(!empty($imageurl)){ ?>
	<img width="324" height="324" src="<?php echo $imageurl[0];?>" class="woocommerce-placeholder wp-post-image" alt="">
	<?php }else{ ?>
		<img width="324" height="324" src="<?php echo site_url();?>/wp-content/uploads/woocommerce-placeholder.png" class="woocommerce-placeholder wp-post-image" alt="Placeholder">
	<?php } ?>
	</td>
    <td><a href=""><?php the_title(); ?></a></td>
    <td class="post-status" data-title="<?php esc_attr_e( 'Status', 'dokan-lite' ); ?>">
        <label class="dokan-label <?php echo esc_attr( dokan_get_post_status_label_class( $post->post_status ) ); ?>"><?php echo esc_html( dokan_get_post_status( $post->post_status ) ); ?></label>
    </td>
	<td data-title="<?php esc_attr_e( 'SKU', 'dokan-lite' ); ?>">
		<?php
		if ( get_post_meta($loop->post->ID, 'sku', true) != '' ) {
			echo get_post_meta($loop->post->ID, 'sku', true);
		} else {
			echo '<span class="na">&ndash;</span>';
		}
		?>
		</td>
    <td data-title="<?php esc_attr_e( 'Stock', 'dokan-lite' ); ?>">
        <?php
       if ( get_post_meta($loop->post->ID, 'in_stock', true) == 'in_stock' ) {
            echo '<mark class="instock">' . esc_html__( 'In stock', 'dokan-lite' ) . '</mark>';
        } else {
            echo '<mark class="outofstock">' . esc_html__( 'Out of stock', 'dokan-lite' ) . '</mark>';
        }

        ?>
    </td>
   <td data-title="<?php esc_attr_e( 'Price', 'dokan-lite' ); ?>">
	<?php
	if ( get_post_meta($loop->post->ID, 'sales_price', true) != '' ) {
		echo get_post_meta($loop->post->ID, 'sales_price', true);
	} else {
		echo '<span class="na">&ndash;</span>';
	}
	?>
	</td>
    <td data-title="<?php esc_attr_e( 'Earning', 'dokan-lite' ); ?>">
	<?php
	if ( get_post_meta($loop->post->ID, 'earning_price', true) != '' ) {
		echo get_post_meta($loop->post->ID, 'earning_price', true);
	} else {
		echo '<span class="na">&ndash;</span>';
	}
	?>
	</td>
    <td data-title="<?php esc_attr_e( 'Type', 'dokan-lite' ); ?>">
        <?php
        if( get_post_meta($loop->post->ID, 'types', true) == 'grouped' ):
            echo '<span class="product-type tips grouped" title="' . esc_html__( 'Grouped', 'dokan-lite' ) . '"></span>';
        elseif ( get_post_meta($loop->post->ID, 'types', true) == 'external' ):
            echo '<span class="product-type tips external" title="' . esc_html__( 'External/Affiliate', 'dokan-lite' ) . '"></span>';
        elseif ( get_post_meta($loop->post->ID, 'types', true) == 'simple' ):

            if ( get_post_meta($loop->post->ID, 'types', true) == 'virtual' ) {
                echo '<span class="product-type tips virtual" title="' . esc_html__( 'Virtual', 'dokan-lite' ) . '"></span>';
            } elseif ( get_post_meta($loop->post->ID, 'types', true) == 'downloadable' ) {
                echo '<span class="product-type tips downloadable" title="' . esc_html__( 'Downloadable', 'dokan-lite' ) . '"></span>';
            } else {
                echo '<span class="product-type tips simple" title="' . esc_html__( 'Simple', 'dokan-lite' ) . '"></span>';
            }

            elseif ( get_post_meta($loop->post->ID, 'types', true) == 'variable' ):
                echo '<span class="product-type tips variable" title="' . esc_html__( 'Variable', 'dokan-lite' ) . '"></span>';
            else:
                // Assuming that we have other types in future
           
        endif;
        ?>
    </td>
    <td data-title="<?php esc_attr_e( 'Views', 'dokan-lite' ); ?>">
        <?php echo (int) get_post_meta( $loop->post->ID, 'pageview', true ); ?>
    </td>
    <td class="post-date" data-title="<?php esc_attr_e( 'Date', 'dokan-lite' ); ?>">
        <?php
        if ( '0000-00-00 00:00:00' == $loop->post->post_date ) {
            $t_time = $h_time = __( 'Unpublished', 'dokan-lite' );
            $time_diff = 0;
        } else {
            $t_time = get_the_time( __( 'Y/m/d g:i:s A', 'dokan-lite' ) );
            $m_time = $loop->post->post_date;
            $time = get_post_time( 'G', true, $post );

            $time_diff = time() - $time;

            if ( $time_diff > 0 && $time_diff < 24 * 60 * 60 ) {
                $h_time = sprintf( __( '%s ago', 'dokan-lite' ), human_time_diff( $time ) );
            } else {
                $h_time = mysql2date( __( 'Y/m/d', 'dokan-lite' ), $m_time );
            }
        }

        $post_date_column_time = apply_filters( 'post_date_column_time', dokan_date_time_format( $h_time, true ), $post, 'date', 'all' );

        echo '<abbr title="' . esc_attr( dokan_date_time_format( $t_time ) ) . '">' . esc_html( $post_date_column_time ) . '</abbr>';
        echo '<div class="status">';
        if ( 'publish' == $loop->post->post_status ) {
            esc_html_e( 'Published', 'dokan-lite' );
        } elseif ( 'future' == $loop->post->post_status ) {
            if ( $time_diff > 0 ) {
                echo '<strong class="attention">' . esc_html__( 'Missed schedule', 'dokan-lite' ) . '</strong>';
            } else {
                esc_html_e( 'Scheduled', 'dokan-lite' );
            }
        } else {
            esc_html_e( 'Last Modified', 'dokan-lite' );
        }
        ?>
        </div>
    </td>
    <td class="diviader"></td>
</tr>
