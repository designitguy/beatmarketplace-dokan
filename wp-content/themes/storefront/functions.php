<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
 
 
 
 add_filter( 'dokan_query_var_filter', 'dokan_load_document_menu' );
function dokan_load_document_menu( $query_vars ) {
	$query_vars['license'] = 'license';
	$query_vars['add-license'] = 'add-license';
	$query_vars['edit-license'] = 'edit-license';
	$query_vars['add-beat'] = 'add-beat';
	$query_vars['edit-beat'] = 'edit-beat';
	$query_vars['beat'] = 'beat';
    return $query_vars;
}
add_filter( 'dokan_get_dashboard_nav', 'dokan_add_license_menu' );
function dokan_add_license_menu( $urls ) {
    $urls['license'] = array(
        'title' => __( 'License & Contracts', 'dokan'),
        'icon'  => '<i class="fa fa-id-card"></i>',
        'url'   => dokan_get_navigation_url( 'license' ),
        'pos'   => 52
	);
	$urls['beat'] = array(
        'title' => __( 'Beats', 'dokan'),
        'icon'  => '<i class="fa fa-id-card"></i>',
        'url'   => dokan_get_navigation_url( 'beat' ),
        'pos'   => 53
    );
    return $urls;
}
add_action( 'dokan_load_custom_template', 'dokan_load_template' );
function dokan_load_template( $query_vars ) {
    if ( isset( $query_vars['license'] ) ) {
        require_once dirname( __FILE__ ) . '/license.php';
        //exit();
	}

	if ( isset( $query_vars['add-license'] ) ) {
        require_once dirname( __FILE__ ) . '/add-license.php';
        //exit();
	}	
	
	if ( isset( $query_vars['edit-license'] ) ) {
        require_once dirname( __FILE__ ) . '/edit-license.php';
        //exit();
	}
	
	if ( isset( $query_vars['add-beat'] ) ) {
        require_once dirname( __FILE__ ) . '/add-beat.php';
        //exit();
	}	
	if ( isset( $query_vars['edit-beat'] ) ) {
        require_once dirname( __FILE__ ) . '/edit-beat.php';
        //exit();
	}	

	if ( isset( $query_vars['beat'] ) ) {
        require_once dirname( __FILE__ ) . '/beat.php';
       
    }
} 


// Add all enaled link
function dokan_license_listing_links() {
	 $args = array( 'post_type' => 'license', 'post_status' => 'publish');
	 	$loop = new WP_Query( $args ); 
		
		 $argsall = array( 'post_type' => 'license');
	 	$loopall = new WP_Query( $argsall ); 
	?>
	<ul class="dokan-listing-filter dokan-left subsubsub">
	
    <li <?php echo $status_class == 'all' ? ' class="active"' : ''; ?>>
        <a href="<?php echo esc_url( $permalink ); ?>"><?php printf( esc_html__( 'All (%d)', 'dokan-lite' ), esc_html( $loopall->post_count ) ); ?></a>
    </li>
   
        <li <?php echo $status_class == 'enabled' ? ' class="active"' : ''; ?>>
            <a href="<?php echo esc_url( add_query_arg( array( 'post_status' => 'publish' ), $permalink ) ); ?>"><?php echo esc_html( 'enabled' ). ' (' . esc_html( $loop->post_count ) . ')'; ?></a>
        </li>
    
</ul> <!-- .post-statuses-filter -->
	<?php
}

// beat litig lik
function dokan_beat_listing_links() {
	
	$args = array( 'post_type' => 'beats', 'post_status' => 'publish');
	 	$loop = new WP_Query( $args ); 
		
		 $argspen = array( 'post_type' => 'beats', 'post_status' => 'pending');
	 	$looppen = new WP_Query( $argspen );

		$argsall = array( 'post_type' => 'beats', 'post_status' => 'pending');
	 	$loopall = new WP_Query( $args ); 		
	?>
	<ul class="dokan-listing-filter dokan-left subsubsub">
	
    <li <?php echo $status_class == 'all' ? ' class="active"' : ''; ?>>
        <a href="<?php echo esc_url( $permalink ); ?>"><?php printf( esc_html__( 'All (%d)', 'dokan-lite' ), esc_html( $loopall->post_count ) ); ?></a>
    </li>
   
        <li <?php echo $status_class == 'online' ? ' class="active"' : ''; ?>>
            <a href="<?php echo esc_url( add_query_arg( array( 'post_status' => 'publish' ), $permalink ) ); ?>"><?php echo esc_html( 'Online' ). ' (' . esc_html( $loop->post_count ) . ')'; ?></a>
        </li>
		
		<li <?php echo $status_class == 'pending' ? ' class="active"' : ''; ?>>
            <a href="<?php echo esc_url( add_query_arg( array( 'post_status' => 'pending' ), $permalink ) ); ?>"><?php echo esc_html( 'Pending review' ). ' (' . esc_html( $looppen->post_count ) . ')'; ?></a>
        </li>
    
</ul> <!-- .post-statuses-filter -->
	<?php
}		

// Beats Filter
function dokan_beat_listing_filter() {
    dokan_get_template_part( 'products/beatfilter' );
}

function dokan_beat_listing_filter_months_dropdown( $user_id ) {
    global $wpdb, $wp_locale;

    $months = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT DISTINCT YEAR( post_date ) AS year, MONTH( post_date ) AS month
            FROM $wpdb->posts
            WHERE post_type = 'beats'
            AND post_author = %d
            ORDER BY post_date DESC",
            $user_id
        )
    );

    /**
     * Filter the 'Months' drop-down results.
     *
     * @since 2.1
     *
     * @param object $months    The months drop-down query results.
     */
    $months = apply_filters( 'months_dropdown_results', $months );

    $month_count = count( $months );

    if ( ! $month_count || ( 1 == $month_count && 0 == $months[0]->month ) ) {
        return;
    }

    $date = isset( $_GET['date'] ) ? (int) $_GET['date'] : 0;
    ?>
    <select name="date" id="filter-by-date" class="dokan-form-control">
        <option<?php selected( $date, 0 ); ?> value="0"><?php esc_html_e( 'All dates', 'dokan-lite' ); ?></option>
    <?php
    foreach ( $months as $arc_row ) {
        if ( 0 == $arc_row->year ) {
            continue;
        }

        $month = zeroise( $arc_row->month, 2 );
        $year = $arc_row->year;

        printf(
            "<option %s value='%s' >%s</option>\n",
            selected( $date, $year . $month, false ),
            esc_attr( $year . $month ),
            /* translators: 1: month name, 2: 4-digit year */
            sprintf( esc_html__( '%1$s %2$d', 'dokan-lite' ), esc_html( $wp_locale->get_month( $month ) ), esc_html( $year ) ) // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
        );
    }
    ?>
    </select>
    <?php
}

add_action( 'template_redirect', 'handle_delete_license'  );
function handle_delete_license(){
	if ( ! is_user_logged_in() ) {
            return;
        }

        if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
            return;
        }

        if ( ! current_user_can( 'dokan_delete_product' ) ) {
            return;
        }

        dokan_delete_license_handler();
}

function dokan_delete_license_handler() {
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'dokan-delete-license' ) {
        $license_id = isset( $_GET['license_id'] ) ? (int) $_GET['license_id'] : 0;
	   
		$getdata = wp_unslash( $_GET );
		 if ( $license_id == 0 ) {
            wp_redirect( add_query_arg( array( 'message' => 'error1' ), dokan_get_navigation_url( 'license' ) ) );
            return;
        }
		/* if ( ! wp_verify_nonce( $getdata['_wpnonce'], 'dokan-delete-license' ) ) {
            wp_redirect( add_query_arg( array( 'message' => 'error2' ), dokan_get_navigation_url( 'license' ) ) );
            return;
        } */
		
		 wp_delete_post( $license_id );
		 
		   wp_redirect( add_query_arg( array( 'message' => 'license_deleted' ), dokan_get_navigation_url( 'license' ) ) );
	}
}


add_action( 'template_redirect', 'handle_delete_beats'  );
function handle_delete_beats(){
	if ( ! is_user_logged_in() ) {
            return;
        }

        if ( ! dokan_is_user_seller( get_current_user_id() ) ) {
            return;
        }

        if ( ! current_user_can( 'dokan_delete_product' ) ) {
            return;
        }

        dokan_delete_beats_handler();
}

function dokan_delete_beats_handler() {
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'dokan-delete-beats' ) {
        $beat_id = isset( $_GET['beat_id'] ) ? (int) $_GET['beat_id'] : 0;
	   
		$getdata = wp_unslash( $_GET );
		 if ( $beat_id == 0 ) {
            wp_redirect( add_query_arg( array( 'message' => 'error' ), dokan_get_navigation_url( 'beat' ) ) );
            return;
        }
		/* if ( ! wp_verify_nonce( $getdata['_wpnonce'], 'dokan-delete-license' ) ) {
            wp_redirect( add_query_arg( array( 'message' => 'error2' ), dokan_get_navigation_url( 'license' ) ) );
            return;
        } */
		
		 wp_delete_post( $beat_id );
		 
		   wp_redirect( add_query_arg( array( 'message' => 'beat_deleted' ), dokan_get_navigation_url( 'beat' ) ) );
	}
}