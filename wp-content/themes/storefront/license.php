<?php
    global $post;
	$poststatus = isset($_GET['post_status']) ? $_GET['post_status'] : 'publish';
?>

<?php do_action( 'dokan_dashboard_wrap_start' ); ?>

<div class="dokan-dashboard-wrap"> 

    <?php

        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked get_dashboard_side_navigation
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_before' );
        ?>

        <div class="dokan-dashboard-content dokan-product-listing">

            <?php

            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked get_dashboard_side_navigation
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_before' );
            do_action( 'dokan_before_listing_product' );
            ?>

            <article class="dokan-product-listing-area">

                <div class="product-listing-top dokan-clearfix">
                    <h1 class="wp-heading-inline add-new-license-text">License & Contracts</h1>
					<span class="dokan-add-product-link">
                            <?php if ( current_user_can( 'dokan_add_product' ) ): ?>
                                <a href="<?php echo get_home_url().'/index.php/dashboard/add-license/'; ?>" class="dokan-btn dokan-btn-theme">
                                    <i class="fa fa-briefcase">&nbsp;</i>
                                    <?php esc_html_e( 'Add new License', 'dokan-lite' ); ?>
                                </a>
                           

                            <?php
                                do_action( 'dokan_after_add_product_btn' );
                            ?>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="dokan-w12">
                    <div class="dokan-w12">
				<div class="dokan-form-inline dokan-w6 dokan-product-date-filter">
					<?php dokan_license_listing_links(); ?>
				</div>
				<form method="get" class="dokan-form-inline dokan-w6 dokan-product-search-form">
					<button type="submit" name="product_listing_search" value="ok" class="dokan-btn dokan-btn-theme">Search</button>
					<input type="hidden" id="dokan_product_search_nonce" name="dokan_product_search_nonce" value="d283f28556"><input type="hidden" name="_wp_http_referer" value="/dokanpaid/index.php/dashboard/products/">
					<div class="dokan-form-group">
						<input type="text" class="dokan-form-control" name="product_search_name" placeholder="Search Products" value="">
					</div>
				</form>
			</div>
                </div>

                <div class="dokan-dashboard-product-listing-wrapper">

                    <form id="product-filter" method="POST" class="dokan-form-inline">
                        <div class="dokan-form-group">
                            <label for="bulk-product-action-selector" class="screen-reader-text"><?php esc_html_e( 'Select bulk action', 'dokan-lite' ); ?></label>
								<select name="status" id="bulk-product-action-selector" class="dokan-form-control chosen">
									<option class="bulk-product-status" value="-1">Bulk Actions</option>
									<option class="bulk-product-status" value="delete">Delete Permanently</option>
								</select>
                            
                        </div>

                        <div class="dokan-form-group">
                            <?php wp_nonce_field( 'bulk_product_status_change', 'security' ); ?>
                            <input type="submit" name="bulk_product_status_change" id="bulk-product-action" class="dokan-btn dokan-btn-theme" value="<?php esc_attr_e( 'Apply', 'dokan-lite' ); ?>">
                        </div>
                        <table class="dokan-table dokan-table-striped product-listing-table dokan-inline-editable-table" id="dokan-product-list-table">
                            <thead>
                                <tr>
                                    <th id="cb" class="manage-column column-cb check-column">
                                        <label for="cb-select-all"></label>
                                        <input id="cb-select-all" class="dokan-checkbox" type="checkbox">
                                    </th>
                                    <th><?php esc_html_e( 'Image', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'License Name', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Status', 'dokan-lite' ); ?></th>
									<th><?php esc_html_e( 'SKU', 'dokan-lite' ); ?></th>  
                                    <th><?php esc_html_e( 'Default Price', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Files', 'dokan-lite' ); ?></th>
                                   
                                    <th><?php esc_html_e( 'Actions', 'dokan-lite' ); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                               $args = array( 'post_type' => 'license', 'post_status' => $poststatus);
								$loop = new WP_Query( $args ); 
                                ?>
								 <?php
									while ( $loop->have_posts() ) : $loop->the_post();
									?>
                                   <tr>
										<td class="dokan-product-select">
											<label for="cb-select-<?php echo esc_attr( $loop->post->ID ); ?>"></label>
											<input class="cb-select-items dokan-checkbox" type="checkbox" name="bulk_products[]" value="<?php echo esc_attr( $loop->post->ID ); ?>">
										</td>
										<td data-title="<?php esc_attr_e( 'Image', 'dokan-lite' ); ?>">
										<?php $imageid = get_post_meta($loop->post->ID, 'license_image', true); 
										$imageurl = wp_get_attachment_image_src($imageid); ?>
										<?php if(!empty($imageurl)){ ?>
										<img width="324" height="324" src="<?php echo $imageurl[0];?>" class="woocommerce-placeholder wp-post-image" alt="">
										<?php }else{ ?>
											<img width="324" height="324" src="<?php echo site_url();?>/wp-content/uploads/woocommerce-placeholder.png" class="woocommerce-placeholder wp-post-image" alt="Placeholder">
										<?php } ?>
										</td>
										<td><a href="<?php echo site_url(); ?>/index.php/dashboard/edit-license/?pid=<?php echo $loop->post->ID; ?>&_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>"><?php the_title(); ?></a></td>
										<td class="post-status" data-title="<?php esc_attr_e( 'Status', 'dokan-lite' ); ?>">
											<label class="dokan-label <?php echo esc_attr( dokan_get_post_status_label_class( $loop->post->post_status ) ); ?>"><?php echo esc_html( dokan_get_post_status( $loop->post->post_status ) ); ?></label>
										</td>
										<td data-title="<?php esc_attr_e( 'SKU', 'dokan-lite' ); ?>">
										<?php
										if ( get_post_meta($loop->post->ID, 'sku', true) != '' ) {
											echo get_post_meta($loop->post->ID, 'sku', true);
										} else {
											echo '<span class="na">&ndash;</span>';
										}
										?>
										</td>
										<td data-title="<?php esc_attr_e( 'Price', 'dokan-lite' ); ?>">
										<?php
										if ( get_post_meta($loop->post->ID, 'sales_price', true) != '' ) {
											echo get_post_meta($loop->post->ID, 'sales_price', true);
										} else {
											echo '<span class="na">&ndash;</span>';
										}
										?>
										</td>
										<td data-title="<?php esc_attr_e( 'FIles', 'dokan-lite' ); ?>">
										<?php
										if ( get_post_meta($loop->post->ID, 'files', true) != '' ) {
											echo get_post_meta($loop->post->ID, 'files', true);
										} else {
											echo '<span class="na">&ndash;</span>';
										}
										?>
										</td>
										<td><a href="<?php echo site_url(); ?>/index.php/dashboard/edit-license/?pid=<?php echo $loop->post->ID; ?>&_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>">Edit</a><br><a href="<?php echo site_url();?>/index.php/dashboard/license/?action=dokan-delete-license&amp;license_id=<?php echo $loop->post->ID; ?>&amp;_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>">Delete</a></td>                
									</tr>
								<?php
									endwhile;
									wp_reset_postdata();
								?>
                            </tbody>

                        </table>
                    </form>
                </div>
                 
                </article>

                <?php

            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked get_dashboard_side_navigation
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_after' );
            do_action( 'dokan_after_listing_product' );
            ?>

        </div><!-- #primary .content-area -->

        <?php

        /**
         *  dokan_dashboard_content_after hook
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_after' );
        ?>

    </div><!-- .dokan-dashboard-wrap -->

<?php do_action( 'dokan_dashboard_wrap_end' ); ?>
