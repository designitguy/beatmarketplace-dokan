<?php
    global $post;
	$poststatus = isset($_GET['post_status']) ? $_GET['post_status'] : 'publish';
?>

<?php do_action( 'dokan_dashboard_wrap_start' ); ?>
 
<div class="dokan-dashboard-wrap">

    <?php

        /**
         *  dokan_dashboard_content_before hook
         *
         *  @hooked get_dashboard_side_navigation
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_before' );
        ?>

        <div class="dokan-dashboard-content dokan-product-listing">

            <?php

            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked get_dashboard_side_navigation
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_before' );
            do_action( 'dokan_before_listing_product' );
            ?>

            <article class="dokan-product-listing-area">

                <div class="product-listing-top dokan-clearfix">
				<h1 class="wp-heading-inline add-new-license-text">Beats</h1>
                    <?php //dokan_product_listing_status_filter(); ?>

                    <?php if ( dokan_is_seller_enabled( get_current_user_id() ) ): ?>
                        <span class="dokan-add-product-link">
                            <?php if ( current_user_can( 'dokan_add_product' ) ): ?>
                                <a href="<?php echo get_home_url().'/index.php/dashboard/add-beat/'; ?>" class="dokan-btn dokan-btn-theme">
                                    <i class="fa fa-briefcase">&nbsp;</i>
                                    <?php esc_html_e( 'Add new beats', 'dokan-lite' ); ?>
                                </a>
                            <?php endif ?>

                            <?php
                                do_action( 'dokan_after_add_product_btn' );
                            ?>
                        </span>
                    <?php endif; ?>
                </div>

                <?php dokan_product_dashboard_errors(); ?>

                <div class="dokan-w12">
                    <?php dokan_beat_listing_filter(); ?>
                </div>
				<div class="dokan-w12">
                    <?php dokan_beat_listing_links(); ?>
                </div>

                <div class="dokan-dashboard-product-listing-wrapper">

                    <form id="product-filter" method="POST" class="dokan-form-inline">
                        <div class="dokan-form-group">
                            <label for="bulk-product-action-selector" class="screen-reader-text"><?php esc_html_e( 'Select bulk action', 'dokan-lite' ); ?></label>

                           <select name="status" id="bulk-product-action-selector" class="dokan-form-control chosen">
									<option class="bulk-product-status" value="-1">Bulk Actions</option>
									<option class="bulk-product-status" value="delete">Delete Permanently</option>
								</select>
                        </div>

                        <div class="dokan-form-group">
                            <?php wp_nonce_field( 'bulk_product_status_change', 'security' ); ?>
                            <input type="submit" name="bulk_product_status_change" id="bulk-product-action" class="dokan-btn dokan-btn-theme" value="<?php esc_attr_e( 'Apply', 'dokan-lite' ); ?>">
                        </div>
                        <table class="dokan-table dokan-table-striped product-listing-table dokan-inline-editable-table" id="dokan-product-list-table">
                            <thead>
                                <tr>
                                    <th id="cb" class="manage-column column-cb check-column">
                                        <label for="cb-select-all"></label>
                                        <input id="cb-select-all" class="dokan-checkbox" type="checkbox">
                                    </th>
                                    <th><?php esc_html_e( 'Image', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Name', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Status', 'dokan-lite' ); ?></th>

                                    <th><?php esc_html_e( 'SKU', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Stock', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Price', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Earning', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Type', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Views', 'dokan-lite' ); ?></th>
                                    <th><?php esc_html_e( 'Date', 'dokan-lite' ); ?></th>
                                </tr>
                            </thead>
                            <tbody>
							<?php
                               $args = array( 'post_type' => 'beats','post_status'=>array($poststatus));
								$loop = new WP_Query( $args ); 
                                ?>
								<?php
								if ( $loop->have_posts() ) {
									while ( $loop->have_posts() ) : $loop->the_post();
									global $post;
									 $row_actions = dokan_product_get_row_action( $post );
									?>
                              <tr class="<?php echo esc_attr( $tr_class ); ?>">
									<td class="dokan-product-select">
										<label for="cb-select-<?php echo esc_attr( $loop->post->ID ); ?>"></label>
										<input class="cb-select-items dokan-checkbox" type="checkbox" name="bulk_products[]" value="<?php echo esc_attr( $loop->post->ID ); ?>">
									</td>
									<td data-title="<?php esc_attr_e( 'Image', 'dokan-lite' ); ?>">
									<?php $imageid = get_post_meta($loop->post->ID, 'license_image', true); 
									$imageurl = wp_get_attachment_image_src($imageid); ?>
									<?php if(!empty($imageurl)){ ?>
									<img width="324" height="324" src="<?php echo $imageurl[0];?>" class="woocommerce-placeholder wp-post-image" alt="">
									<?php }else{ ?>
										<img width="324" height="324" src="<?php echo site_url();?>/wp-content/uploads/woocommerce-placeholder.png" class="woocommerce-placeholder wp-post-image" alt="Placeholder">
									<?php } ?>
									</td>
									<td>
									<p><a href="<?php echo site_url(); ?>/index.php/dashboard/edit-beat/?pid=<?php echo $loop->post->ID; ?>&_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>"><?php the_title(); ?></a></p>
									<div class="row-actions">
										<a href="<?php echo site_url(); ?>/index.php/dashboard/edit-beat/?pid=<?php echo $loop->post->ID; ?>&_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>">Edit</a> | <a href="<?php echo site_url();?>/index.php/dashboard/beat/?action=dokan-delete-beats&amp;beat_id=<?php echo $loop->post->ID; ?>&amp;_wpnonce=<?php echo wp_create_nonce($loop->post->ID); ?>">Delete Permanently</a> | <a href="<?php echo site_url();?>/index.php/beats/travis-scott-type/">View</a> | <a href="#quick-edit">Quick Edit</a> | <a href="<?php echo site_url();?>/index.php/dashboard/beat/?action=dokan-duplicate-beats&amp;beat_id=<?php echo $loop->post->ID; ?>">Duplicate</a>									</div>
									</td>
									<td class="post-status" data-title="<?php esc_attr_e( 'Status', 'dokan-lite' ); ?>">
										<label class="dokan-label <?php echo esc_attr( dokan_get_post_status_label_class( $post->post_status ) ); ?>"><?php echo esc_html( dokan_get_post_status( $post->post_status ) ); ?></label>
									</td>
									<td data-title="<?php esc_attr_e( 'SKU', 'dokan-lite' ); ?>">
										<?php
										if ( get_post_meta($loop->post->ID, 'sku', true) != '' ) {
											echo get_post_meta($loop->post->ID, 'sku', true);
										} else {
											echo '<span class="na">&ndash;</span>';
										}
										?>
										</td>
									<td data-title="<?php esc_attr_e( 'Stock', 'dokan-lite' ); ?>">
										<?php
										
									   if ( get_post_meta($loop->post->ID, 'stock', true) == 'in_stock' ) {
											echo '<mark class="instock">' . esc_html__( 'In stock', 'dokan-lite' ) . '</mark>';
										} else {
											echo '<mark class="outofstock">' . esc_html__( 'Out of stock', 'dokan-lite' ) . '</mark>';
										}

										?>
									</td>
								   <td data-title="<?php esc_attr_e( 'Price', 'dokan-lite' ); ?>">
									<?php
									if ( get_post_meta($loop->post->ID, 'sales_price', true) != '' ) {
										echo get_post_meta($loop->post->ID, 'sales_price', true);
									} else {
										echo '<span class="na">&ndash;</span>';
									}
									?>
									</td>
									<td data-title="<?php esc_attr_e( 'Earning', 'dokan-lite' ); ?>">
									<?php
									if ( get_post_meta($loop->post->ID, 'earning_price', true) != '' ) {
										echo get_post_meta($loop->post->ID, 'earning_price', true);
									} else {
										echo '<span class="na">&ndash;</span>';
									}
									?>
									</td>
									<td data-title="<?php esc_attr_e( 'Type', 'dokan-lite' ); ?>">
										<?php
										if( get_post_meta($loop->post->ID, 'types', true) == 'grouped' ):
											echo '<span class="product-type tips grouped" title="' . esc_html__( 'Grouped', 'dokan-lite' ) . '"></span>';
										elseif ( get_post_meta($loop->post->ID, 'types', true) == 'external' ):
											echo '<span class="product-type tips external" title="' . esc_html__( 'External/Affiliate', 'dokan-lite' ) . '"></span>';
										elseif ( get_post_meta($loop->post->ID, 'types', true) == 'simple' ):

											if ( get_post_meta($loop->post->ID, 'types', true) == 'virtual' ) {
												echo '<span class="product-type tips virtual" title="' . esc_html__( 'Virtual', 'dokan-lite' ) . '"></span>';
											} elseif ( get_post_meta($loop->post->ID, 'types', true) == 'downloadable' ) {
												echo '<span class="product-type tips downloadable" title="' . esc_html__( 'Downloadable', 'dokan-lite' ) . '"></span>';
											} else {
												echo '<span class="product-type tips simple" title="' . esc_html__( 'Simple', 'dokan-lite' ) . '"></span>';
											}

											elseif ( get_post_meta($loop->post->ID, 'types', true) == 'variable' ):
												echo '<span class="product-type tips variable" title="' . esc_html__( 'Variable', 'dokan-lite' ) . '"></span>';
											else:
												// Assuming that we have other types in future
										   
										endif;
										?>
									</td>
									<td data-title="<?php esc_attr_e( 'Views', 'dokan-lite' ); ?>">
										<?php echo (int) get_post_meta( $loop->post->ID, 'pageview', true ); ?>
									</td>
									<td class="post-date" data-title="<?php esc_attr_e( 'Date', 'dokan-lite' ); ?>">
										<?php
										if ( '0000-00-00 00:00:00' == $loop->post->post_date ) {
											$t_time = $h_time = __( 'Unpublished', 'dokan-lite' );
											$time_diff = 0;
										} else {
											$t_time = get_the_time( __( 'Y/m/d g:i:s A', 'dokan-lite' ) );
											$m_time = $loop->post->post_date;
											$time = get_post_time( 'G', true, $post );

											$time_diff = time() - $time;

											if ( $time_diff > 0 && $time_diff < 24 * 60 * 60 ) {
												$h_time = sprintf( __( '%s ago', 'dokan-lite' ), human_time_diff( $time ) );
											} else {
												$h_time = mysql2date( __( 'Y/m/d', 'dokan-lite' ), $m_time );
											}
										}

										$post_date_column_time = apply_filters( 'post_date_column_time', dokan_date_time_format( $h_time, true ), $post, 'date', 'all' );

										echo '<abbr title="' . esc_attr( dokan_date_time_format( $t_time ) ) . '">' . esc_html( $post_date_column_time ) . '</abbr>';
										echo '<div class="status">';
										if ( 'publish' == $loop->post->post_status ) {
											esc_html_e( 'Published', 'dokan-lite' );
										} elseif ( 'future' == $loop->post->post_status ) {
											if ( $time_diff > 0 ) {
												echo '<strong class="attention">' . esc_html__( 'Missed schedule', 'dokan-lite' ) . '</strong>';
											} else {
												esc_html_e( 'Scheduled', 'dokan-lite' );
											}
										} else {
											esc_html_e( 'Last Modified', 'dokan-lite' );
										}
										?>
										</div>
									</td>
									<td class="diviader"></td>
								</tr>
								 <?php
									endwhile;
									wp_reset_postdata();
									} else {
								?>
								 <tr>
                                        <td colspan="7"><?php esc_html_e( 'No beat found', 'dokan-lite' ); ?></td>
                                    </tr>
                                <?php } ?>
								
                            </tbody>

                        </table>
                    </form>
                </div>
                   
                </article>

                <?php

            /**
             *  dokan_dashboard_content_before hook
             *
             *  @hooked get_dashboard_side_navigation
             *
             *  @since 2.4
             */
            do_action( 'dokan_dashboard_content_inside_after' );
            do_action( 'dokan_after_listing_product' );
            ?>

        </div><!-- #primary .content-area -->

        <?php

        /**
         *  dokan_dashboard_content_after hook
         *
         *  @since 2.4
         */
        do_action( 'dokan_dashboard_content_after' );
        ?>

    </div><!-- .dokan-dashboard-wrap -->

<?php do_action( 'dokan_dashboard_wrap_end' ); ?>
